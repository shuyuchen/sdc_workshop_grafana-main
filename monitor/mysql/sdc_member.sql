DROP DATABASE IF EXISTS `sdc_demo`;
CREATE DATABASE `sdc_demo`;
USE `sdc_demo`;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `sdc_member`;
CREATE TABLE `sdc_member` (
  `member_id` varchar(10) NOT NULL,
  `member_name` varchar(32) DEFAULT NULL,
  `member_section` varchar(100) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sdc_member` WRITE;

INSERT INTO `sdc_member` (`member_id`, `member_name`, `member_section`, `remark`)
VALUES ('003481','Kevin Tang','Technology Development and Education and Training Section','Need support!!!'),
('003454','Dennis Chung','Technology Development and Education and Training','He is tall, rich and good-looking');

UNLOCK TABLES;

GRANT ALL PRIVILEGES ON sdc_demo.* TO 'admin'@'%';