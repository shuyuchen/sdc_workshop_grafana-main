# 執行環境
 - Macbook M2晶片
   - 有必要記得要改docker-compose.yml
 - docker
 - maven
 - jdk 1.8
 - spring-boot 2.6.2

### 動作開始, 執行build, 把基本img ready
```
$ docker-compose build
$ docker-compose up -d
```

### 測試開機是否應用可以使用
```
$ curl http://localhost:8080/api/get_sdc_member/003454
```

### 登入 Grafana
```
$ http://localhost:3000
acc/pw : admin/admin
登入要改密碼可以skip
```

### Grafana Dashborad
```
spring-boot-observability
==> https://grafana.com/grafana/dashboards/17175

Spring Boot 2.1 System Monitor
==>https://grafana.com/grafana/dashboards/11378

如果沒有網路可以自己去找JSON 匯入
```

### 打入大量資料看dashboard 變化
```
$ k6 run --vus 100 --iterations 1500 --duration 20s k6_test.js
```

### 資料清空
```
$ docker volume ls
$ docker volume rm {volume_id}

$ docker network ls
$ docker network rm {network_id}
