package com.demo.service;

import com.demo.mapper.SDC_MemberMapper;
import com.demo.model.SDC_Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class SDC_MemberService {
  private static final Logger logger = LoggerFactory.getLogger(SDC_MemberService.class);

  @Autowired
  private SDC_MemberMapper sdc_memberMapper;

  public List<SDC_Member> getUsers() {
    return sdc_memberMapper.getAll();
  }

  public SDC_Member getSdc_member(String member_id) {
    logger.info("recrived id from api rest: {}", member_id);

    SDC_Member sdc_member = sdc_memberMapper.get(member_id);

    logger.info("return user to api rest: {}", sdc_member);
    return sdc_member;
  }

  public void save(SDC_Member sdc_member) {
    sdc_memberMapper.insert(sdc_member);
  }

  public void update(SDC_Member sdc_member) {
    sdc_memberMapper.update(sdc_member);
  }

  public void delete(@PathVariable("member_id") String member_id) {
    sdc_memberMapper.delete(member_id);
  }
}
