package com.demo.mapper;

import com.demo.model.SDC_Member;

import java.util.List;

public interface SDC_MemberMapper {

  List<SDC_Member> getAll();

  SDC_Member get(String member_id);

  void insert(SDC_Member sdc_member);

  void update(SDC_Member sdc_member);

  void delete(String member_id);

}
