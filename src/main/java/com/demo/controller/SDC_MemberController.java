package com.demo.controller;

import com.demo.model.SDC_Member;
import com.demo.service.SDC_MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/get_sdc_member")
public class SDC_MemberController {
  
  @Autowired
  private SDC_MemberService sdc_memberService;

  @GetMapping(value = "/{member_id}")
  public SDC_Member getUser(@PathVariable(name = "member_id", required = true) String member_id) {
    return sdc_memberService.getSdc_member(member_id);
  }

  @PostMapping
  public void save(@RequestBody SDC_Member sdc_member) {
    sdc_memberService.save(sdc_member);
  }

  @PutMapping
  public void update(@RequestBody SDC_Member sdc_member) {
    sdc_memberService.update(sdc_member);
  }

  @DeleteMapping(value = "/{member_id}")
  public void delete(@PathVariable("member_id") String member_id) {
    sdc_memberService.delete(member_id);
  }
}
