package com.demo.model;

import java.io.Serializable;

public class SDC_Member implements Serializable {

  private static final long serialVersionUID = 1L;
  private String member_id;
  private String member_name;
  private String member_section;
  private String remark;

  public SDC_Member() {
    super();
  }

  public SDC_Member(String member_name, String member_section, String remark) {
    super();
    this.member_name = member_name;
    this.member_section = member_section;
    this.remark = remark;
  }

  public String getMember_id() {
    return member_id;
  }

  public void setMember_id(String member_id) {
    this.member_id = member_id;
  }

  public String getMember_name() {
    return member_name;
  }

  public void setMember_name(String member_name) {
    this.member_name = member_name;
  }

  public String getMember_section() {
    return member_section;
  }

  public void setMember_section(String member_section) {
    this.member_section = member_section;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  @Override
  public String toString() {
    return "member_name " + this.member_name + ", member_section " + this.member_section + ", remark " + this.remark;
  }

}
